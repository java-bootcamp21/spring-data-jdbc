package com.jdbc.basic;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.jdbc.basic.entity.Player;
import com.jdbc.basic.service.PlayerService;


public class BasicDemo implements CommandLineRunner {
	
	private final Logger logger = LoggerFactory.getLogger(BasicDemo.class);
	private final PlayerService playerService;
	

	public BasicDemo(PlayerService playerService) {
		super();
		this.playerService = playerService;
	}



	@Override
	public void run(String... args) throws Exception {
		
		
		logger.info("player with id 1: {}",playerService.findById(1));
		Player player = new Player("Player 7","Albania",LocalDateTime.now(),1);
		playerService.create(player);
		logger.info("{}",playerService.findAll());
		
		Player p1 = playerService.findById(5);
		p1.setName("Player 5.1");
		p1.setNationality("Kosovo");
		playerService.update(5, p1);
		logger.info("{}",playerService.findAll());
		playerService.delete(4);
		logger.info("{}",playerService.findAll());
		
		
	}

}
