package com.jdbc.basic.service;

import java.util.List;

import com.jdbc.basic.entity.Player;

public interface PlayerService {
	
	Player findById(Integer id) throws Exception;
	List<Player> findAll()throws Exception;
	Integer create(Player params)throws Exception;
	Integer update(Integer playerId, Player player)throws Exception;
	void delete(Integer id)throws Exception;

}
