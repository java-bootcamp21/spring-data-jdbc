package com.jdbc.basic.repository;

import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.jdbc.basic.entity.Player;

@Repository
public class PlayerRepositoryImpl implements PlayerRepository{

	private final JdbcTemplate template;
	
	public PlayerRepositoryImpl(JdbcTemplate template) {
		super();
		this.template = template;
	}

	@Override
	public Player findById(String query, Object... params) throws Exception {
		return template.queryForObject(query,
				BeanPropertyRowMapper.newInstance(Player.class),params);
	}

	@Override
	public List<Player> findAll(String query) throws Exception {
		return template.query(query, BeanPropertyRowMapper
				.newInstance(Player.class));
	}

	@Override
	public Integer create(String query, Object... parms) throws Exception {
		return template.update(query,parms);
	}

	@Override
	public Integer update(String query, Object... params) throws Exception {
		return template.update(query,params);
	}

	@Override
	public void delete(String query, Object... params) throws Exception {
		template.update(query,params);	
	}

	

}
