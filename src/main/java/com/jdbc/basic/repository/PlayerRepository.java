package com.jdbc.basic.repository;

import java.util.List;

import com.jdbc.basic.entity.Player;

public interface PlayerRepository {
	
	Player findById(String query,Object... params) throws Exception;
	List<Player> findAll(String query)throws Exception;
	Integer create(String query,Object... parms)throws Exception;
	Integer update(String query, Object... params)throws Exception;
	void delete(String query,Object... params)throws Exception;

}
