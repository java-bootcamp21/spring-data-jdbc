package com.jdbc.dao.entity;

import java.time.LocalDateTime;

public class Player extends BaseEntity<Integer>{
	
	private Integer id;
	private String name;
	private String nationality;
	private LocalDateTime birthDate;
	private Integer titles;
	
	
	
	public Player() {
		super();
	}
	public Player(String name, String nationality, LocalDateTime bithDate, Integer titles) {
		super();
		this.name = name;
		this.nationality = nationality;
		this.birthDate = bithDate;
		this.titles = titles;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNationality() {
		return nationality;
	}
	public void setNationality(String nationality) {
		this.nationality = nationality;
	}
	public LocalDateTime getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(LocalDateTime bithDate) {
		this.birthDate = bithDate;
	}
	public Integer getTitles() {
		return titles;
	}
	public void setTitles(Integer titles) {
		this.titles = titles;
	}
	@Override
	public String toString() {
		return "Player [id=" + id + ", name=" + name + ", nationality=" + nationality + ", bithDate=" + birthDate
				+ ", titles=" + titles + "]";
	}
	

	public void setId(Integer id) {
		this.id = id;
	}
	@Override
	public Integer getId() {
		return id;
	}
	
	
	
	

}
