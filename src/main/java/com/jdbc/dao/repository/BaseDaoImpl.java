package com.jdbc.dao.repository;import java.io.Serializable;
import java.util.List;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.jdbc.dao.entity.BaseEntity;

public abstract class BaseDaoImpl<T extends BaseEntity, I extends Serializable>{

	private final Class<T> type;
	
	protected abstract JdbcTemplate getJdbcTemplate();
	
	public BaseDaoImpl(Class<T> type) {
		this.type = type;
	}

	public T findById(String query, Object... params) throws Exception {
		return getJdbcTemplate().queryForObject(query, 
				BeanPropertyRowMapper.newInstance(type),params);
	}

	public List<T> findAll(String query) throws Exception {
		return getJdbcTemplate().query(query,BeanPropertyRowMapper.newInstance(type));
	}

	public I create(String query, Object... parms) throws Exception {
		return (I)(Object) getJdbcTemplate()
				.update(query,parms);
	}

	public I update(String query, Object... params) throws Exception {
		return (I)(Object) getJdbcTemplate()
				.update(query,params);
	}

	public void delete(String query, Object... params) throws Exception {
		getJdbcTemplate().update(query,params);
		
	}

	
	
}
