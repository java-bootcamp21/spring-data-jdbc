package com.jdbc.dao.repository;

import java.io.Serializable;
import java.util.List;

import com.jdbc.dao.entity.BaseEntity;

public interface BaseDao<T extends BaseEntity, I extends Serializable>{
	
	T findById(String query,Object... params) throws Exception;
	List<T> findAll(String query)throws Exception;
	I create(String query,Object... parms)throws Exception;
	I update(String query, Object... params)throws Exception;
	void delete(String query,Object... params)throws Exception;

}
