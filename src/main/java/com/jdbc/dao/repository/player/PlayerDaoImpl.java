package com.jdbc.dao.repository.player;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.jdbc.dao.entity.Player;
import com.jdbc.dao.repository.BaseDaoImpl;

@Service
public class PlayerDaoImpl extends BaseDaoImpl<Player, Integer> implements PlayerDao{

	private final JdbcTemplate jdbcTemplate;
	
	public PlayerDaoImpl(JdbcTemplate jdbcTemplate) {
		super(Player.class);
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	protected JdbcTemplate getJdbcTemplate() {
		return this.jdbcTemplate;
	}

}
