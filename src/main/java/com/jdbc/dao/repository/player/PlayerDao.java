package com.jdbc.dao.repository.player;

import com.jdbc.dao.entity.Player;
import com.jdbc.dao.repository.BaseDao;

public interface PlayerDao extends BaseDao<Player,Integer>{

}
