package com.jdbc.dao.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.jdbc.dao.entity.Player;
import com.jdbc.dao.repository.player.PlayerDao;

@Service
public class PlayerServiceImpl implements PlayerService{
	
	private static String Q_PLAYER_FIND_ALL = "select * from player";
	private static String Q_PLAYER_FIND_BY_ID = "select * from player where id=?";
	private static String Q_PLAYER_CREATE = "insert into player(name,nationality,birth_date,titles) values (?,?,?,?)";
	private static String Q_PLAYER_UPDATE = "update player set name=?,nationality=?,birth_date=?,titles=? where id=?";
	private static String Q_PLAYER_DELETE = "delete from player where id=?";
			
	
	private final PlayerDao repository;
	

	public PlayerServiceImpl(PlayerDao repository) {
		super();
		this.repository = repository;
	}

	@Override
	public Player findById(Integer id) throws Exception {
		return repository.findById(Q_PLAYER_FIND_BY_ID, id);
	}

	@Override
	public List<Player> findAll() throws Exception {
		return repository.findAll(Q_PLAYER_FIND_ALL);
	}

	@Override
	public Integer create(Player player) throws Exception {
		return repository.create(Q_PLAYER_CREATE,player.getName(),
				player.getNationality(),player.getBirthDate(),player.getTitles());
	}

	@Override
	public Integer update(Integer playerId, Player player) throws Exception {
		return repository.update(Q_PLAYER_UPDATE,player.getName(),
				player.getNationality(),player.getBirthDate(),player.getTitles(),playerId);
	}

	@Override
	public void delete(Integer id) throws Exception {
		repository.delete(Q_PLAYER_DELETE, id);
		
	}

}
